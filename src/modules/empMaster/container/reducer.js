import { CHANGE } from "./actions";
import { empList, empListHeader, empTableWidth } from '../seed';

export const initialState = {
    employees: empList,
    employeeDetails: {
        empFormDetails: {
            empId: '',
            firstName: '',
            lastName: '',
            dob: '',
            gender: '',
            maritalStatus: '',
            offContactNo: '',
            offMailId: '',
            personalContactNo: '',
            personalMailId: '',
            pfNumber: '',
            panNumber: '',
            idProof: '',
            attachedProof: '',
            commAddress: '',
            commPinCode: '',
            commCity: '',
            commState: '',
            commDistrict: '',
            permAddress: '',
            permPinCode: '',
            permCity: '',
            permState: '',
            permDistrict: ''
        },
        empJobDetails: {
            empId: '',
            type: '',
            doj: '',
            dojEffDate: '',
            workLoc: '',
            workLocEffDate: '',
            dpt: '',
            dptEffDate: '',
            designation: '',
            designationEffDate: '',
            grade: '',
            gradeEffDate: '',
            reportingPerson: '',
            reportingPersonEffDate: '',
            addReportingPerson: '',
            addReportingPersonEffDate: ''
        },
        empEduDetails: {
            instituteName: '',
            examOrBoard: '',
            fromDate: '',
            toDate: '',
            percenage: '',
            specialization: '',
            certificate: '',
            uploadedCertificate: ''
        },
        empWorkExperience: {
            organizationName: '',
            jobTitle: '',
            fromDate: '',
            toDate: ''
        },
        empFamilyDetails: {
            firstName: '',
            lastName: '',
            relationship: '',
            emgContactNo: '',
            nomineePercentage: '',
        },
        empHealthRecords: {
            bloodGroup: '',
            differentlyAbled: '',
            percentage: '',
            ailment: '',
            details: '',
            emergency: '',
            insurencePolicies: []
        }
    },
    insurencePolicy: {
        policyNo: '',
        provider: '',
        document: '',
    },
    updateId: ''
}

export const EmpMasterReducer = (state = initialState, action) => {
    switch (action.type) {
        case CHANGE:
            return { ...state, [action.payload.name]: action.payload.value }
        default:
            return state;
    }
}
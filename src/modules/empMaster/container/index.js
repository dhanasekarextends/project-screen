import React, { Component } from 'react';
import EmpList from '../components/empList';
import { Route, Switch } from 'react-router-dom';
import EmpDetails from '../components/empDetails';
import EmpFullDetails from '../components/empFullDetails';
import { connect } from 'react-redux';
import { bindActionCreators } from "redux";
import * as EmpMasterActions from './actions';
import BreadComb from '../../../commonComponents/breadComb';

class EmpMaster extends Component {
    render() {
        return (
            <div>
                <Switch>
                    <Route path='/employees' exact render={() => <EmpList {...this.props} />} />
                    <Route path='/employees/form' render={() => <EmpDetails  {...this.props} />} />
                    <Route path='/employees/personal-details' render={() => <EmpFullDetails {...this.props} />} />
                </Switch>
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        empData: state.EmpMasterReducer,
    }
}

const mapDispatchToProps = (dispatch) => {
    return bindActionCreators({
        fieldOnChange: EmpMasterActions.fieldOnChange,
        moveToArray: EmpMasterActions.moveToArray,
        changeUpdateId: EmpMasterActions.changeUpdateId,
        editOnClick: EmpMasterActions.editOnClick,
        deleteOnClick: EmpMasterActions.deleteOnClick
    }, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(EmpMaster);
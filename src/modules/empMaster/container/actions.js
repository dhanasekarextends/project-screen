import EmpDetails from "../components/empDetails";
import { isNewExpression } from "@babel/types";

export const CHANGE = "FORM_CHANGE"
export const handleChange = (name, value) => ({
    type: CHANGE, payload: { name: name, value: value }
});

export const fieldOnChange = (stateKey, key, value) => {
    return async (dispatch, getState) => {
        const { employeeDetails } = getState().EmpMasterReducer;
        switch (stateKey) {
            case "empFormDetails":
                employeeDetails[stateKey][key] = value;
                dispatch(handleChange('employeeDetails', { ...employeeDetails }));
                break;

            case "empJobDetails":
                employeeDetails[stateKey][key] = value;
                dispatch(handleChange('employeeDetails', employeeDetails));
                break;

            case "empEduDetails":
                employeeDetails[stateKey][key] = value;
                dispatch(handleChange('employeeDetails', employeeDetails));
                break;

            case "empWorkExperience":
                employeeDetails[stateKey][key] = value;
                dispatch(handleChange('employeeDetails', employeeDetails));
                break;

            case "empFamilyDetails":
                employeeDetails[stateKey][key] = value;
                dispatch(handleChange('employeeDetails', employeeDetails));
                break;

            case "empHealthRecords":
                employeeDetails[stateKey][key] = value;
                dispatch(handleChange('employeeDetails', employeeDetails));
                break;

        }
    }
}

export const moveToArray = () => {
    return async (dispatch, getState) => {
        const { employeeDetails, employees, updateId } = getState().EmpMasterReducer;
        if (updateId) {
            employees[updateId] = { ...employeeDetails }
        } else {
            employees.push({ ...employeeDetails })
        }
        dispatch(handleChange('updateId', employees.length))
        dispatch(handleChange('employees', employees));
    }
}

export const changeUpdateId = (id) => {
    console.log('Returned Id is ' + id);
    return async (dispatch, getState) => {
        const { employees, updateId } = getState().EmpMasterReducer;
        if (updateId === '') {
            id = employees.length;
        }
        console.log('updateId ' + id);
        dispatch(handleChange('updateId', id));
    }
}

export const editOnClick = (index) => {
    console.log(index + 'edited');
    return async (dispatch, getState) => {
        const { employees, updateId } = getState().EmpMasterReducer;
        dispatch(handleChange('employeeDetails', employees[index]))
    }
}

export const deleteOnClick = (index) => {
    console.log(index + 'deleted');
    return async (dispatch, getState) => {
        let { employees } = getState.EmpMasterReducer; {
            employees.splice(index,1);
            dispatch(handleChange('employees', employees));
        }
    }
}
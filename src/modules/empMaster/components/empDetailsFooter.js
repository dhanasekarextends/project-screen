import React from 'react';
import Button from '../../../commonComponents/button';
import { NavLink } from 'react-router-dom';

const EmpDetailsFooter = (props) => {
    return (
        <div className='empDetailFooterContainer'>
            <NavLink to='/employees'><Button bgColor='#f1f1f1' color='#0000008a' buttonName='BACK' onClick={props.backOnClick}/></NavLink>
            <NavLink to='/employees'><Button bgColor='#6cc865' color='white' buttonName='Next' onClick={props.nextOnClick} /></NavLink>
        </div>
    );
};

export default EmpDetailsFooter;
import React from 'react';
import Button from '../../../commonComponents/button';
import InputField from '../../../commonComponents/inputField';
import { jobDetailInputs } from '../seed';
import { history } from '../../../store';

const EmpJobDetails = (props) => {

    const { fieldOnChange, empData } = props;

    let empJobInputFields = jobDetailInputs.map((item, index) => {
        let value = empData.employeeDetails.empJobDetails[item.key];
        return (
            <InputField key={index} data={item} style={{ width: item.width }}
                onChange={(key, value) => fieldOnChange("empJobDetails", key, value)}
                value={value}
            />
        )
    })

    let textFieldValidation = (state) => {
        let isEmpty;
        console.log(empData)
        Object.keys(empData.employeeDetails[state]).map(key => {
            isEmpty = empData.employeeDetails[state][key] === "" ? true : false;
            if (isEmpty) {
                return;
            }
        });
        if (isEmpty) {
            props.moveToArray();
            alert('Please Fill All The Details')
        } else {
            
            history.push('/employees')
        }
    }

    return (
        <div className='inputContainer'>
            {empJobInputFields}
            <div className='updateBtnContainer'>
                <Button buttonName='UPDATE NOW' color='white' bgColor='#3E74FF'
                    onClick={() => textFieldValidation('empJobDetails')} />
            </div>
        </div>
    );
};

export default EmpJobDetails;
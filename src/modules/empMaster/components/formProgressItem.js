import React from 'react';
import { NavLink } from 'react-router-dom';

const FormProgressItem = (props) => {
    return (
        <div className='formProgressItemContainer'>
            <div className='numberTextContainer'>
                <div className='numberStyle'>{props.progressNo}</div>
                <NavLink to='/employees/personal-details/edu-details'><div className='formName'>{props.progressName}</div></NavLink>
            </div>
            {props.progressLine ? <div className='verticalProgressLine' /> : null}
        </div>
    )
}

export default FormProgressItem;
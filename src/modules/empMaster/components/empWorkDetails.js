import React from 'react';
import Button from '../../../commonComponents/button';
import InputField from '../../../commonComponents/inputField';
import { workDetailInputs } from '../seed';

const EmpWorkDetails = (props) => {

    let empWorkInputFields = workDetailInputs.map((item, index) => {
        const { fieldOnChange, empData } = props;
        let value = empData.employeeDetails.empWorkDetails[item.key];
        return (
            <InputField key={index} data={item} style={{ width: item.width }} 
            onChange={(key, value) => fieldOnChange("empWorkDetails", key, value)}
            value={value}/>
        )
    })

    return (
        <div className='inputContainer'>
            {empWorkInputFields}
            <div className='updateBtnContainer'>
                <Button buttonName='UPDATE NOW' color='white' bgColor='#3E74FF' />
            </div>
        </div>
    );
};

export default EmpWorkDetails;
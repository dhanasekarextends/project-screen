import React from 'react';
import Button from '../../../commonComponents/button';
import { NavLink } from 'react-router-dom';

const Footer = (props) => {
    return (
        <div className='footerContainer'>
            <NavLink to='/employees'><Button bgColor='#f1f1f1' color='#0000008a' buttonName='CANCEL' /></NavLink>
            <Button bgColor='#6cc865' color='white' buttonName='SAVE' onClick={props.saveOnClick} />
            <NavLink to='/employees/personal-details'>
                <Button bgColor='#4780FF' color='white' buttonName='SAVE AND CONTINUE' onClick={props.saveAndContinueOnClick} />
            </NavLink>
        </div>
    );
};

export default Footer;
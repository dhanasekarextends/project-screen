import React from 'react';
import BreadComb from '../../../commonComponents/breadComb';
import { empDetailInputs, empCommAddrFields, empPermAddrFields, marketDetails } from '../seed';
import '../seed';
import InputField from '../../../commonComponents/inputField';
import HorizontalDivider from '../../../commonComponents/horizontalDivider';
import Button from '../../../commonComponents/button';
import Footer from './footer';
import { history } from '../../../store';

const EmpDetails = (props) => {
    console.log(props)
    const { fieldOnChange, empData } = props;

    let textFieldValidation = (state) => {
        let isEmpty;
        console.log(empData.employeeDetails[state])
        Object.keys(empData.employeeDetails[state]).map(key => {
            isEmpty = empData.employeeDetails[state][key] === "" ? true : false;
            if (isEmpty) {
                return;
            } else {
                console.log(key, empData.employeeDetails[state][key], isEmpty)
            }
        });
        if (isEmpty) {
            alert('Please Fill All The Details')
        } else {
            props.moveToArray();
            history.push('/employees')
        }
    }

    let inputFields = empDetailInputs.map((item, index) => {
        let value = empData.employeeDetails.empFormDetails[item.key];
        return (
            <InputField key={index} data={item} style={{ width: item.width }}
                onChange={(key, value) => fieldOnChange("empFormDetails", key, value)}
                value={value}
            />
        )
    });

    let commAddField = empCommAddrFields.map((item, index) => {
        return (
            <InputField key={index} data={item} style={{ width: item.width }} onChange={fieldOnChange}
                onChange={(key, value) => fieldOnChange("empFormDetails", key, value)} />
        )
    })

    let permAddField = empPermAddrFields.map((item, index) => {
        return (
            <InputField key={index} data={item} style={{ width: item.width }} onChange={fieldOnChange}
                onChange={(key, value) => fieldOnChange("empFormDetails", key, value)} />
        )
    })

    let divisionField = marketDetails.map((item, index) => {
        return (
            <InputField key={index} data={item} style={{ width: item.width }} onChange={fieldOnChange}
                onChange={(key, value) => fieldOnChange("empFormDetails", key, value)} />
        )
    })

    return (
        <div className='pageContainer'>
            <BreadComb text='My Employees > Form' />
            <div className='inputContainer'>
                {inputFields}
                <HorizontalDivider />
                {commAddField}
                <HorizontalDivider />
                {permAddField}
            </div>
            <Button buttonName='Add Market Detail' color='white' bgColor='#3E74FF' />
            <div className='inputContainer'>
                <div className='divisionStyle'>
                    {divisionField}
                    <span className='addMarkets'>Add Markets</span>
                </div>
            </div>
            <Footer saveOnClick={() => textFieldValidation('empFormDetails')}
                saveAndContinueOnClick={() => console.log('saveAndContinueOnClick')} />
        </div>
    );
};

export default EmpDetails;

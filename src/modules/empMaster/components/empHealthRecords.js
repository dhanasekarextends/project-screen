import React from 'react';
import Button from '../../../commonComponents/button';
import InputField from '../../../commonComponents/inputField';
import { healthRecordInputs, empInsInputs } from '../seed';

const EmpHealthRecords = (props) => {

    let empHealthInputFields = healthRecordInputs.map((item, index) => {
        const { fieldOnChange, empData } = props;
        let value = empData.employeeDetails.empHealthRecords[item.key];
        return (
            <InputField key={index} data={item} style={{ width: item.width }}
                onChange={(key, value) => fieldOnChange("empHealthRecords", key, value)}
                value={value} />
        )
    })

    let empInsInputFields = empInsInputs.map((item, index) => {
        return (
            <InputField key={index} data={item} style={{ width: item.width }} />
        )
    })

    return (
        <div>
            <div className='inputContainer'>
                {empHealthInputFields}
            </div>
            <div className='inputContainer' style={{ marginTop: 10 }}>
                {empInsInputFields}
                <div className='updateBtnContainer'>
                    <Button buttonName='UPDATE NOW' color='white' bgColor='#3E74FF' />
                </div>
            </div>
        </div>
    );
};

export default EmpHealthRecords;
import React from 'react';
import EmpTable from '../../../commonComponents/table';
import { empList, empListHeader, empTableWidth } from '../seed';
import BreadComb from '../../../commonComponents/breadComb';
import FloatingAddButton from '../../../commonComponents/floatingAddButton';
import { NavLink } from 'react-router-dom';

const EmpList = (props) => {
    return (
        <div className='pageContainer'>
            <BreadComb text='My Employees' />
            <EmpTable tableWidth={empTableWidth} tableHeader={empListHeader} tableContent={props.empData.employees} 
            {...props}/>
            <NavLink to='/employees/form' exact activeStyle={{ borderBottom: "2px solid white" }}
                exact><FloatingAddButton onClick={()=> props.changeUpdateId('')}/></NavLink>
        </div>
    );
};

export default EmpList;
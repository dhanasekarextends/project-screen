import React from 'react';
import Button from '../../../commonComponents/button';
import InputField from '../../../commonComponents/inputField';
import { familyDetailInputs } from '../seed';

const EmpFamilyDetails = (props) => {

    let empFamilyInputFields = familyDetailInputs.map((item, index) => {
        const { fieldOnChange, empData } = props;
        let value = empData.employeeDetails.empFamilyDetails[item.key];
        return (
            <InputField key={index} data={item} style={{ width: item.width }} 
            onChange={(key, value) => fieldOnChange("empFamilyDetails", key, value)}
            value={value}/>
        )
    })

    return (
        <div className='inputContainer'>
            {empFamilyInputFields}
            <div className='updateBtnContainer'>
                <Button buttonName='UPDATE NOW' color='white' bgColor='#3E74FF' />
            </div>
        </div>
    );
};

export default EmpFamilyDetails;
import React from 'react';
import Button from '../../../commonComponents/button';
import InputField from '../../../commonComponents/inputField';
import { eduDetailInputs } from '../seed';

const EmpEduDetails = (props) => {

    let empJobInputFields = eduDetailInputs.map((item, index) => {
        const { fieldOnChange, empData } = props;
        let value = empData.employeeDetails.empEduDetails[item.key];
        return (
            <InputField key={index} data={item} style={{ width: item.width }}
                onChange={(key, value) => fieldOnChange("empEduDetails", key, value)}
                value={value} />
        )
    })

    return (
        <div className='inputContainer'>
            {empJobInputFields}
            <div className='updateBtnContainer'>
                <Button buttonName='UPDATE NOW' color='white' bgColor='#3E74FF' />
            </div>
        </div>
    );
};

export default EmpEduDetails;
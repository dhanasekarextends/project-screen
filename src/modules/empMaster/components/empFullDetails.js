import React, { Component } from 'react';
import BreadComb from '../../../commonComponents/breadComb';
import EmpDetailsFooter from './empDetailsFooter';
import { Route, Switch } from 'react-router-dom';
import Table from '../../../commonComponents/table';
import { workExperience, workExpHeader, workExpTableWidth, progressList } from '../seed';
import FormProgressItem from './formProgressItem';
import { ReactComponent as UpLoadIcon } from '../../../assets/upload.svg';
import { ReactComponent as DeleteIcon } from '../../../assets/delete.svg';
import { EmpFullDetailsRoute } from '../routes';

class EmpFullDetails extends Component {

    render() {

        let progress = progressList.map((item, index) => {
            return (
                <FormProgressItem key={index} progressNo={index + 1} progressName={item.name}
                    progressLine={index < progressList.length - 1 ? true : false}
                    completed={true} inProgress={true} />
            )
        });

        let progressRoutes = EmpFullDetailsRoute.map((item, index) => {
            return (
                <Route key={item.path} path={item.path} exact render={() => <item.component {...this.props}
                 updateOnPress={() => console.log('updateOnPress')} />} />
            )
        })

        return (
            <div className='pageContainer'>
                <BreadComb text='My Employees > Employee Form' />
                <div className='formContainer'>
                    <div className='progressAndDpContainer'>
                        <div className='formProgress'>
                            {progress}
                        </div>
                        <div className='dpContainer'>
                            <div className='dpStyle'>

                            </div>
                            <div className='dpNameContainer'>
                                <span className='dpName'>
                                    new_picture.png
                            </span>
                                <div className='dpUploadIcon'>
                                    <UpLoadIcon />
                                    <DeleteIcon />
                                </div>
                            </div>
                        </div>
                    </div>
                    <Switch>
                        {progressRoutes}
                    </Switch>
                </div>
                <Table tableWidth={workExpTableWidth} tableHeader={workExpHeader} tableContent={workExperience} />
                <EmpDetailsFooter backOnClick={() => console.log('backOnClick')} nextOnClick={() => console.log('nextOnClick')} />
            </div>
        );
    }
}



export default EmpFullDetails;
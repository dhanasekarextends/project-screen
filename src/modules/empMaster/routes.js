import React from 'react';
import * as Path from './routePath';
import EmpJobDetails from './components/empJobdetails';
import EmpEduDetails from './components/empEduDetails';
import EmpWorkDetails from './components/empWorkDetails';
import EmpFamilyDetails from './components/empFamilyDetails';
import EmpHealthRecords from './components/empHealthRecords';

export const EmpFullDetailsRoute = [
    {
        component: EmpJobDetails,
        path: Path.EmpJobDetails
    },
    {
        component: EmpEduDetails,
        path: Path.EmpEduDetails
    },
    {
        component: EmpWorkDetails,
        path: Path.EmpWorkDetails
    },
    {
        component: EmpFamilyDetails,
        path: Path.EmpFamilyDetails
    },
    {
        component: EmpHealthRecords,
        path: Path.EmpHealthRecords
    },
];
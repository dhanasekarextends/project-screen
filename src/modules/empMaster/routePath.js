export const EmpJobDetails = '/employees/personal-details';
export const EmpEduDetails = '/employees/personal-details/edu-details';
export const EmpWorkDetails = '/employees/personal-details/work-experience';
export const EmpFamilyDetails = '/employees/personal-details/family-details';
export const EmpHealthRecords = '/employees/personal-details/health-records';
export const EmpList = '/employees';
export const EmpDetails = '/employees/form';
export const EmpFUllDetails = '/employees/personal-details';
export const empList = [
    {
        empID: 'E001',
        empName: 'Dhanasekar S',
        contactNumber: '8489111279',
        role: 'dev',
        reportingTo: 'Sam',
    }, {
        empID: 'E002',
        empName: 'Rohith S',
        contactNumber: '8489111279',
        role: 'dev',
        reportingTo: 'Sam'
    }, {
        empID: 'E003',
        empName: 'Sharp S',
        contactNumber: '8489111279',
        role: 'dev',
        reportingTo: 'Sam'
    }, {
        empID: 'E004',
        empName: 'Mislead S',
        contactNumber: '8489111279',
        role: 'dev',
        reportingTo: 'Sam'
    },
];

export const empTableDispData = ['empId', 'empName', 'offContactNo', 'permCity', 'permState'];

export const empListHeader = [
    {
        id: '1',
        name: 'Employee ID',
    },
    {
        id: '2',
        name: 'Name',
    },
    {
        id: '3',
        name: 'Contact No',
    },
    {
        id: '4',
        name: 'Role',
    },
    {
        id: '5',
        name: 'Reporting To',
    },

];

export const empTableWidth = ['12%', '18%', '18%', '18%', '18%'];

export const empDetailInputs = [
    {
        id: '1',
        name: 'Employee ID',
        key: 'empId',
        width: '30.30%',
        type: 'text',
        placeholder: 'Employee ID'
    },
    {
        id: '2',
        name: 'First Name',
        key: 'firstName',
        width: '30.30%',
        type: 'text'
    },
    {
        id: '3',
        name: 'Last Name',
        key: 'lastName',
        width: '30.30%',
        type: 'text'

    },
    {
        id: '4',
        name: 'Date Of Birth',
        key: 'dob',
        width: '30.30%',
        type: 'text'
    },
    {
        id: '5',
        name: 'Gender',
        key: 'gender',
        width: '30.30%',
        type: 'text'
    },
    {
        id: '6',
        name: 'Marital Status',
        key: 'maritalStatus',
        width: '30.30%',
        type: 'text'
    },
    {
        id: '7',
        name: 'Official Contact No',
        key: 'offContactNo',
        width: '30.30%',
        type: 'text'
    },
    {
        id: '8',
        name: 'Official Mail ID',
        key: 'offMailId',
        width: '30.30%',
        type: 'text'
    },
    {
        id: '9',
        name: 'Personal Contact No',
        key: 'personalContactNo',
        width: '30.30%',
        type: 'text'
    },
    {
        id: '10',
        name: 'Personal Mail ID',
        key: 'personalMailId',
        width: '30.30%',
        type: 'text'
    },
    {
        id: '11',
        name: 'PF Number',
        key: 'pfNumber',
        width: '30.30%',
        type: 'text'
    },
    {
        id: '12',
        name: 'PAN Number',
        key: 'panNumber',
        width: '30.30%',
        type: 'text'
    },
    {
        id: '13',
        name: 'ID Proof',
        key: 'idProof',
        width: '30.30%',
        type: 'text'
    },
    {
        id: '14',
        name: 'Attach Your Proof',
        key: 'attachedProof',
        width: '30.30%',
        type: 'file'
    },

];

export const empCommAddrFields = [
    {
        id: '01',
        name: 'Communication Address',
        key: 'commAddress',
        width: '63.45%',
        type: 'text'
    }, {
        id: '02',
        name: 'Pin Code',
        key: 'commPinCode',
        width: '30.30%',
        type: 'text'
    }, {
        id: '03',
        name: 'City',
        key: 'commCity',
        width: '30.30%',
        type: 'text'
    }, {
        id: '04',
        name: 'State',
        key: 'commState',
        width: '30.30%',
        type: 'text'
    }, {
        id: '05',
        name: 'District',
        key: 'commDistrict',
        width: '30.30%',
        type: 'text'
    },

];

export const empPermAddrFields = [
    {
        id: '01',
        name: 'Permanent Address',
        key: 'permAddress',
        width: '63.45%',
        type: 'text'
    }, {
        id: '02',
        name: 'Pin Code',
        key: 'permPinCode',
        width: '30.30%',
        type: 'text'
    }, {
        id: '03',
        name: 'City',
        key: 'permCity',
        width: '30.30%',
        type: 'text'
    }, {
        id: '04',
        name: 'State',
        key: 'permState',
        width: '30.30%',
        type: 'text'
    }, {
        id: '05',
        name: 'District',
        key: 'permDistrict',
        width: '30.30%',
        type: 'text'
    },

];

export const marketDetails = [
    {
        id: '01',
        name: 'Select Division',
        key: 'selectDivision',
        width: '30.30%',
        type: 'text'
    }
];

export const jobDetailInputs = [
    {
        id: '01',
        name: 'Employee ID',
        key: 'empId',
        width: '46%',
        type: 'text'
    },
    {
        id: '02',
        name: 'Employee Type',
        key: 'type',
        width: '46%',
        type: 'text'
    },
    {
        id: '03',
        name: 'Date Of Joining',
        key: 'doj',
        width: '46%',
        type: 'text'
    },
    {
        id: '04',
        name: 'Effective Date',
        key: 'dojEffDate',
        width: '46%',
        type: 'text'
    },
    {
        id: '05',
        name: 'Work Location',
        key: 'workLoc',
        width: '46%',
        type: 'text'
    },
    {
        id: '06',
        name: 'Effective Date',
        key: 'workLocEffDate',
        width: '46%',
        type: 'text'
    },
    {
        id: '07',
        name: 'Department',
        key: 'dpt',
        width: '46%',
        type: 'text'
    },
    {
        id: '08',
        name: 'Effective Date',
        key: 'dptEffDate',
        width: '46%',
        type: 'text'
    },
    {
        id: '09',
        name: 'Designation',
        key: 'designation',
        width: '46%',
        type: 'text'
    },
    {
        id: '10',
        name: 'Effective Date',
        key: 'designitionEffDate',
        width: '46%',
        type: 'text'
    },
    {
        id: '11',
        name: 'Grade',
        key: 'grade',
        width: '46%',
        type: 'text'
    }
    ,
    {
        id: '12',
        name: 'Effective Date',
        key: 'gradeEffDate',
        width: '46%',
        type: 'text'
    },
    {
        id: '13',
        name: 'Reporting Person',
        key: 'reportingPerson',
        width: '46%',
        type: 'text'
    },
    {
        id: '14',
        name: 'Effective Date',
        key: 'reportingPersonEffDate',
        width: '46%',
        type: 'text'
    },
    {
        id: '15',
        name: 'Additional Reporting Person',
        key: 'addReportingPerson',
        width: '46%',
        type: 'text'
    },
    {
        id: '16',
        name: 'Effective Date',
        key: 'addReportingPersonEffDate',
        width: '46%',
        type: 'text'
    },
];

export const workExpHeader = [
    {
        id: '01',
        name: 'Effective From'
    }, {
        id: '01',
        name: 'Grade'
    }, {
        id: '01',
        name: 'Department'
    }, {
        id: '01',
        name: 'Designation'
    }, {
        id: '01',
        name: 'Reporting Person'
    }, {
        id: '01',
        name: 'Working Location'
    },
];

export const workExpTableWidth = ['15%', '15%', '15%', '15%', '15%', '15%'];

export const workExperience = [
    {
        id: '1',
        effectiveFrom: '23 Oct 2015',
        grade: 'G1',
        department: 'Software',
        designation: 'Developer',
        reportingPerson: 'Andrew',
        workLocation: 'California'
    },
    {
        id: '2',
        effectiveFrom: '30 Oct 2015',
        grade: 'G1',
        department: 'Software',
        designation: 'Testing',
        reportingPerson: 'Andrew',
        workLocation: 'California'
    },
    {
        id: '3',
        effectiveFrom: '12 Oct 2015',
        grade: 'G1',
        department: 'Software',
        designation: 'DevOps',
        reportingPerson: 'Andrew',
        workLocation: 'California'
    }
];

export const formProgress = [
    {
        id: '1',
        name: 'Job Details'
    }, {
        id: '2',
        name: 'Education Details'
    }, {
        id: '3',
        name: 'Work Experience'
    }, {
        id: '4',
        name: 'Family And Nominee Details'
    }, {
        id: '5',
        name: 'Health Records'
    },
];

export const eduDetailInputs = [
    {
        id: '01',
        name: 'School/College Name',
        key: 'instituteName',
        width: '46%',
        type: 'text'
    },
    {
        id: '02',
        name: 'Examination/Board',
        key: 'examOrBoard',
        width: '46%',
        type: 'text'
    },
    {
        id: '03',
        name: 'From Month And Year',
        key: 'fromDate',
        width: '46%',
        type: 'text'
    },
    {
        id: '04',
        name: 'To Month And Year',
        key: 'toDate',
        width: '46%',
        type: 'text'
    },
    {
        id: '05',
        name: 'Percentage',
        key: 'percentage',
        width: '46%',
        type: 'text'
    },
    {
        id: '06',
        name: 'Specialization',
        key: 'specialization',
        width: '46%',
        type: 'text'
    },
    {
        id: '07',
        name: 'Certificate',
        key: 'certificate',
        width: '46%',
        type: 'text'
    },
    {
        id: '08',
        name: 'Upload Certificate',
        key: 'uploadedCertificate',
        width: '46%',
        type: 'file'
    }
]

export const workDetailInputs = [
    {
        id: '01',
        name: 'Organization Name',
        key: 'organizationName',
        width: '46%',
        type: 'text'
    }, {
        id: '02',
        name: 'Job Title',
        key: 'jobTitle',
        width: '46%',
        type: 'text'
    }, {
        id: '03',
        name: 'From',
        key: 'fromDate',
        width: '46%',
        type: 'text'
    }, {
        id: '04',
        name: 'To',
        key: 'toDate',
        width: '46%',
        type: 'text'
    },
];

export const familyDetailInputs = [
    {
        id: '01',
        name: 'First Name',
        key: 'firstName',
        width: '46%',
        type: 'text'
    }, {
        id: '02',
        name: 'Last Name',
        key: 'lastName',
        width: '46%',
        type: 'text'
    }, {
        id: '03',
        name: 'Relationship',
        key: 'relationship',
        width: '46%',
        type: 'text'
    }, {
        id: '04',
        name: 'Emergency Contact No',
        key: 'emgContactNo',
        width: '46%',
        type: 'text'
    }, {
        id: '05',
        name: 'Nominee Percentage',
        key: 'nomineePercentage',
        width: '46%',
        type: 'text'
    },
]

export const healthRecordInputs = [
    {
        id: '01',
        name: 'Blood Group',
        key: 'bloodGroup',
        width: '46%',
        type: 'text'
    }, {
        id: '02',
        name: 'Differently Abled',
        key: 'differentlyAbled',
        width: '46%',
        type: 'text'
    }, {
        id: '03',
        name: 'Percentage',
        key: 'percentage',
        width: '46%',
        type: 'text'
    }, {
        id: '04',
        name: 'Ailment',
        key: 'ailment',
        width: '46%',
        type: 'text'
    }, {
        id: '05',
        name: 'Details',
        key: 'details',
        width: '46%',
        type: 'text'
    }, {
        id: '06',
        name: 'Emergency',
        key: 'emergency',
        width: '46%',
        type: 'text'
    }
]

export const empInsInputs = [
    {
        id: '01',
        name: 'Insurance Policy No',
        key: 'policyNo',
        width: '46%',
        type: 'text'
    },
    {
        id: '02',
        name: 'Provider',
        key: 'provider',
        width: '46%',
        type: 'text'
    },
    {
        id: '03',
        name: 'Policy Document',
        key: 'document',
        width: '46%',
        type: 'text'
    }
];

export const progressList = [
    {
        id: '01',
        name: 'Job Details'
    },
    {
        id: '02',
        name: 'Education Details'
    },
    {
        id: '03',
        name: 'Work Experience'
    },
    {
        id: '04',
        name: 'Family And Nominee Details'
    },
    {
        id: '05',
        name: 'Health Records'
    }
]
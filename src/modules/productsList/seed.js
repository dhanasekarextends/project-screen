export const products = [
    {
        id: '1',
        number: 'P001',
        name: 'Hide And Seek',
        cgst: '5%',
        sgst: '5%',
        igst: '',
        uom: [
            {
                id: '1',
                sku: 'P001-1',
                unit: '1gm',
                price: '25',
            },
            {
                id: '2',
                sku: 'P001-2',
                unit: '1gm',
                price: '25',
            }, {
                id: '3',
                sku: 'P001-3',
                unit: '1gm',
                price: '25',
            },
        ]
    },
    {
        id: '2',
        number: 'P002',
        name: 'Bourbon',
        cgst: '5%',
        sgst: '5%',
        igst: '',
        uom: [
            {
                id: '1',
                sku: 'P002-1',
                unit: '2gm',
                price: '25',
            },
            {
                id: '2',
                sku: 'P002-2',
                unit: '2gm',
                price: '25',
            },
            {
                id: '3',
                sku: 'P002-3',
                unit: '2gm',
                price: '25',
            },
        ]
    },
    {
        id: '3',
        number: 'P003',
        name: 'GoodDay-Chocolate',
        cgst: '5%',
        sgst: '5%',
        igst: '',
        uom: [
            {
                id: '1',
                sku: 'P003-1',
                unit: '3gm',
                price: '25',
            },
            {
                id: '2',
                sku: 'P003-2',
                unit: '3gm',
                price: '25',
            },
            {
                id: '3',
                sku: 'P003-3',
                unit: '3gm',
                price: '25',
            },

        ]
    },
];


export const productHeader = [
    { id: '0', name: 'Product No' },
    { id: '1', name: 'Product Name' },
    { id: '2', name: 'CGST' },
    { id: '3', name: 'SGST' },
    { id: '4', name: 'IGST' },
]

export const uomHeader = [
    { id: '0', name: 'UOM' },
    { id: '1', name: 'Price' },
]

export const productTableWidth = ['15%', '25%', '10%', '10%', '10%'];
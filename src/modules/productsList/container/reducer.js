import { CHANGE } from "./actions";
import { products } from '../seed';

export const initialState = {
    isUomModalVisible: false,
    selectedUom: 0,
    productDetails: {
        number: '',
        name: '',
        categoryName: '',
        cgst: '',
        sgst: '',
        igst: '',
        divisionName: '',
        description: '',
        uom: []
    },
    products: products,
}

export const ProductListReducer = (state = initialState, action) => {
    switch (action.type) {
        case CHANGE:
            return { ...state, [action.payload.name]: action.payload.value }
        default:
            return state;
    }
}
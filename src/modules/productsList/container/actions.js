export const CHANGE = "FORM_CHANGE"
export const handleChange = (name, value) => ({
    type: CHANGE, payload: { name: name, value: value }
})

export const showUomModal = (index) => {
    return async (dispatch) => {
        dispatch(handleChange('isUomModalVisible', true));
        dispatch(handleChange('selectedUom', index));
    }
}

export const toggleUomModal = (index) => {
    return async (dispatch, getState) => {
        let {isUomModalVisible} = getState().ProductListReducer;
        if (index) {
            dispatch(handleChange('selectedUom', index));
        }
        dispatch(handleChange('isUomModalVisible', !isUomModalVisible));
    }
}

export const deleteOnClick = (index) => {
    return async (dispatch, getState) => {
        let {products} = getState().ProductListReducer;
        products.splice(index, 1)
        dispatch(handleChange('products', products));

    }
}

export const editOnClick = (index) => {
    return async (dispatch, getState) => {
        let { productDetails, products } = getState().ProductListReducer;
        productDetails = products[index];
        dispatch(handleChange('productDetails', productDetails));
        dispatch(handleChange('products', products));
    }
}

export const closeModal = () => {
    return async (dispatch) => {
        dispatch(handleChange('isUomModalVisible', false));
    }
}
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from "redux";
import * as ProductListActions from './actions';
import BreadComb from '../../../commonComponents/breadComb';
import { productHeader, uomHeader, productTableWidth } from '../seed';
import ProductTable from '../../../commonComponents/table';
import Modal from 'react-modal';

class ProductScreen extends Component {

    render() {
        const { productListValues, toggleUomModal, editOnClick, deleteOnClick } = this.props;
        let uom = productListValues.products.length > 0 ? 
        productListValues.products[productListValues.selectedUom].uom : [];
        return (
            <div className='pageContainer'>
                <Modal
                    isOpen={productListValues.isUomModalVisible}
                    onRequestClose={()=>toggleUomModal()}
                    style={customStyles}
                    contentLabel="Example Modal"
                    ariaHideApp={false}
                >
                    <ProductTable tableHeader={uomHeader} tableContent={uom}
                    tableWidth={['120px', '120px']} type='uom'/>
                </Modal> 
                <BreadComb text={'Products'} />
                <ProductTable tableHeader={productHeader} tableContent={productListValues.products}
                    onClick={(index) => toggleUomModal(index)} tableWidth={productTableWidth}
                    deleteOnClick={(index) => deleteOnClick(index)} editOnClick={(index) => editOnClick(index)} />
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        productListValues: state.ProductListReducer,
    }
}

const mapDispatchToProps = (dispatch) => {
    return bindActionCreators({
        toggleUomModal: ProductListActions.toggleUomModal,
        deleteOnClick: ProductListActions.deleteOnClick,
        editOnClick: ProductListActions.editOnClick,
    }, dispatch)
}

const customStyles = {
    content: {
        top: '50%',
        left: '50%',
        right: 'auto',
        bottom: 'auto',
        marginRight: '-50%',
        transform: 'translate(-50%, -50%)'
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(ProductScreen);
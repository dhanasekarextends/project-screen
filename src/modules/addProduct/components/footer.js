import React from 'react';
import Button from '../../../commonComponents/button';

const Footer = (props) => {
    return (
        <div className='footerContainer'>
            <Button bgColor='#f1f1f1' color='#0000008a' buttonName='CANCEL' />
            <Button bgColor='#6cc865' color='white' buttonName='SAVE' onClick={props.saveOnClick}/>
        </div>
    );
};

export default Footer;
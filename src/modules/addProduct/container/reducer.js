import { CHANGE } from "./actions";

export const initialState = {

}

export const ProductScreenReducer = (state = initialState, action) => {
    switch (action.type) {
        case CHANGE:
            return { ...state, [action.payload.name]: action.payload.value }
        default:
            return state;
    }
}
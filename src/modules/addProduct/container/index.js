import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from "redux";
import * as NewProductActions from './actions';
import BreadComb from '../../../commonComponents/breadComb';
import { productFormInputs, formInputWidth, uomFormInputs, uomInputWidth } from '../seed';
import Button from '../../../commonComponents/button';
import RoundDeleteButton from '../../../commonComponents/roundDeleteButton';
import InputField from '../../../commonComponents/inputField';
import Footer from '../components/footer';


class NewProduct extends Component {
    render() {
        const { fieldOnChange, saveOnPress, newProductValues,addUom, uomOnChange } = this.props;
        let productInputFields = productFormInputs.map((item, index) => {
            return (
                <InputField key={index} value={newProductValues.productDetails[item.key]}
                    style={{ width: formInputWidth[index] }} data={item} onClick={fieldOnChange} />
            )
        })

        let uomInputFields = uomFormInputs.map((item, index) => {
            return (
                <InputField key={index} style={{ width: uomInputWidth[index] }} data={item} onClick={uomOnChange} 
                onChange={uomOnChange}/>
            )
        })

        let uomList = newProductValues.productDetails.uom.map((item, index) => {
            return (
                <div className='uomRowContainer'>
                    {uomInputFields}
                    <RoundDeleteButton />
                </div>)
        })

        return (
            <div className='pageContainer'>
                <BreadComb text='My Products > Product Form' />
                <div className='inputContainer'>
                    {productInputFields}
                </div>
                <Button bgColor='#3e74ff' buttonName='Add UOM' color='white' onClick={addUom}/>
                {newProductValues.productDetails.uom.length > 0 ? <div className='inputContainer'>
                    {uomList}
                </div> : null}
                <Footer saveOnClick={()=>saveOnPress(this.props.history)} />
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        newProductValues: state.ProductListReducer,
    }
}

const mapDispatchToProps = (dispatch) => {
    return bindActionCreators({
        fieldOnChange: NewProductActions.fieldOnChange,
        saveOnPress: NewProductActions.saveOnPress,
        addUom: NewProductActions.addUom,
        uomOnChange: NewProductActions.uomOnChange
    }, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(NewProduct);
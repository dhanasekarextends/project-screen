export const CHANGE = "FORM_CHANGE"
export const handleChange = (name, value) => ({
    type: CHANGE, payload: { name: name, value: value }
})

export const fieldOnChange = (key, value) => {
    return async (dispatch, getState) => {
        const { productDetails } = getState().ProductListReducer;
        productDetails[key] = value;
        dispatch(handleChange('productDetails', productDetails));
    }
}

export const addUom = () => {
    return async (dispatch, getState) => {
        const { productDetails } = getState().ProductListReducer;
        const productUom = {
            sku: '',
            unitOfMeasurement: '',
            price: '',
        }
        productDetails.uom.push({ ...productUom });
        dispatch((handleChange("productDetails", productDetails)));
    }
}

export const saveOnPress = (history) => {
    return async (dispatch, getState) => {
        const { productDetails, products, productUom } = getState().ProductListReducer;
        let isEmpty = false;
        let uomIsEmpty = false;
        Object.keys(productDetails).map((item, index) => {
            if (item !== 'uom') {
                isEmpty = productDetails[item] === '' ? true : isEmpty;
            } else {
                productDetails[item].map((uom, i) => {
                    Object.keys(uom).map((uomItem, key) => {
                        uomIsEmpty = uom[uomItem] === '' ? true : false;
                    })
                })
            }
        })
        
        if (isEmpty && uomIsEmpty) {
            alert('please fill all the fields')
        } else {
            
            products.push({ ...productDetails });
            Object.keys(productDetails).map((item) => {
                if (item !== 'uom')
                    productDetails[item] = ''
            })
            dispatch(handleChange('productDetails', productDetails));
            dispatch(handleChange('products', products));
            history.push('/');
        }
    }
}

export const uomOnChange = (key, index, value) => {
    return async (dispatch, getState) => {
        // console.log('hello ' + key, index, value)
    }
}

export const products = [
    {
        id: '1',
        number: 'P001',
        name: 'Hide And Seek',
        cgst: '5%',
        sgst: '5%',
        igst: '',
        uom: [
            {
                id: '1',
                sku: 'P001-1',
                unit: '1gm',
                price: '25',
            },
            {
                id: '2',
                sku: 'P001-2',
                unit: '1gm',
                price: '25',
            }, {
                id: '3',
                sku: 'P001-3',
                unit: '1gm',
                price: '25',
            },
        ]
    },
    {
        id: '2',
        number: 'P002',
        name: 'Bourbon',
        cgst: '5%',
        sgst: '5%',
        igst: '',
        uom: [
            {
                id: '1',
                sku: 'P002-1',
                unit: '2gm',
                price: '25',
            },
            {
                id: '2',
                sku: 'P002-2',
                unit: '2gm',
                price: '25',
            },
            {
                id: '3',
                sku: 'P002-3',
                unit: '2gm',
                price: '25',
            },
        ]
    },
    {
        id: '3',
        number: 'P003',
        name: 'GoodDay-Chocolate',
        cgst: '5%',
        sgst: '5%',
        igst: '',
        uom: [
            {
                id: '1',
                sku: 'P003-1',
                unit: '3gm',
                price: '25',
            },
            {
                id: '2',
                sku: 'P003-2',
                unit: '3gm',
                price: '25',
            },
            {
                id: '3',
                sku: 'P003-3',
                unit: '3gm',
                price: '25',
            },

        ]
    },
];

export const productFormInputs = [
    {
        id: '1',
        name: 'Product No',
        key: 'number',
        type: 'text'
    },
    {
        id: '2',
        name: 'Product Name',
        key: 'name',
        type: 'text'
    },
    {
        id: '3',
        name: 'Category Name',
        key: 'categoryName',
        type: 'text'
    },
    {
        id: '4',
        name: 'CGST',
        key: 'cgst',
        type: 'text'
    },
    {
        id: '4',
        name: 'SGST',
        key: 'sgst',
        type: 'text'
    },
    {
        id: '5',
        name: 'IGST',
        key: 'igst',
        type: 'text'
    },
    {
        id: '6',
        name: 'Division Name',
        key: 'divisionName',
        type: 'text'
    },
    {
        id: '7',
        name: 'Description',
        key: 'description',
        type: 'text'
    }
]

export const uomFormInputs = [
    {
        id: '1',
        name: 'SKU',
        key: 'sku',
        type: 'text'
    },
    {
        id: '2',
        name: 'Unit Of Measurement',
        key: 'unitOfMeasurement',
        type: 'text'
    },
    {
        id: '3',
        name: 'Price',
        key: 'price',
        type: 'text'
    },
]

export const formInputWidth = ['30.30%', '30.30%', '30.30%', '30.30%', '30.30%', '30.30%', '30.30%', '63.30%'];

export const uomInputWidth = ['15.30%', '30.30%', '30.30%'];


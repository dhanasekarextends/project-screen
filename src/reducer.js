import { combineReducers } from "redux";
import { ProductScreenReducer } from "./modules/addProduct/container/reducer";
import { ProductListReducer } from "./modules/productsList/container/reducer";
import { EmpMasterReducer } from "./modules/empMaster/container/reducer";

export const allReducer = combineReducers({
    ProductScreenReducer,
    ProductListReducer,
    EmpMasterReducer
})
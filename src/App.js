import React from 'react';
import './App.css';
import Header from './commonComponents/header';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import NewProductScreen from './modules/addProduct/container/index';
import ProductListScreen from './modules/productsList/container/index';
import EmployeeScreen from './modules/empMaster/container/index';
import './styles/styles.css';

function App() {
  return (
    <div className='appContainer'>
      <Header />
      <Route path='/' component={ProductListScreen} exact />
      {/* <Route path='/new-product' component={NewProductScreen} /> */}
      <Route path='/employees' component={EmployeeScreen} />
    </div>
  );
}

export default App;
import React from 'react';

const InputField = (props) => {
    return (
        <fieldset className='fieldSetStyle' style={props.style} >
            <legend className='fieldSetLegend'>{props.data.name}</legend>
            <input type={props.data.type} className='fieldSetInput' value={props.value}
                onChange={(event) => props.onChange(props.data.key, event.target.value)} />
        </fieldset>
    );
};

export default InputField;
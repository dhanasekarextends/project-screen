import React from 'react';

const BreadComb = (props) => {
    return (
        <div className='breadCombContainer'>
            <span className='breadComb'>{props.text}</span>
            <div className='helpTextContainer'>
                <span className='helpText'>? Help Text</span>
            </div>
        </div>
    );
};

export default BreadComb;
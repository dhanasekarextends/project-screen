import React from 'react';
import '../styles/styles.css';

const Button = (props) => {
    return (
        <div className='button' style={{backgroundColor: props.bgColor, color: props.color}} onClick={props.onClick}>
            <span className='unselectable'>{props.buttonName}</span>
        </div>
    );
};

export default Button;
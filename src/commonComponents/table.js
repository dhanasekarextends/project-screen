import React from 'react';
import { ReactComponent as EditIcon } from '../assets/edit.svg';
import { ReactComponent as DeleteIcon } from '../assets/delete.svg';

const Table = (props) => {
    let tableHeader = () => {
        return (
            <tr className='productTableHeader'>
                {props.tableHeader.map((item, index) => {
                    return (
                        <th className='productTableHeader' width={props.tableWidth[index]} key={index}>{item.name}</th>
                    )
                })}
                <th></th>
            </tr>
        )
    }

    let tableContent = () => {
        return (
            props.tableContent.length > 0 ?
                props.tableContent.map((item, index) => {

                    return (
                        <tr className='productTr' key={index}>
                            {Object.keys(item).map((list, i) => {
                                if (list !== 'uom' && list !== 'id' && list !== 'sku' &&
                                    list !== 'categoryName' && list !== 'divisionName' && list !== 'description') {
                                    return (
                                        <td onClick={() => list === 'name' ? props.onClick(index) : null}
                                            className={list === 'name' ? 'productName' : 'productTd'}
                                            key={i}>{item[list] ? item[list] : 'N/A'}</td>
                                    )
                                }
                            })}
                            {props.type === 'uom' ? null : <td className='editAndDelete'>
                                <EditIcon className='tableIcons' onClick={() => props.editOnClick(index)} />
                                <DeleteIcon className='tableIcons' onClick={() => props.deleteOnClick(index)} />
                            </td>}
                        </tr>
                    )
                }) : null
        )
    }

    return (
        <table className='productTable'>
            <thead>
                {tableHeader()}
            </thead>
            <tbody>
                {tableContent()}
            </tbody>
        </table>
    );
};

export default Table;
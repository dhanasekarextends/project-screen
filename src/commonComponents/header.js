import React from 'react';
import '../styles/styles.css';
import { NavLink } from 'react-router-dom';
import HeaderLogo from './headerLogo';

const Header = () => {
    return (
        <div className='headerContainer'>
            <HeaderLogo />
            <ul className='headerNavigationContainer'>
                <li className="headerNavigation"><NavLink to='/' activeStyle={{ borderBottom: "2px solid white" }}
                    exact>Product List</NavLink></li>
                <li className='headerNavigation'><NavLink to='/new-product' activeStyle={{ borderBottom: "2px solid white" }}
                    exact>New product</NavLink></li>
                <li className='headerNavigation'><NavLink to='/employees' activeStyle={{ borderBottom: "2px solid white" }}
                    exact>My Employees</NavLink></li>
            </ul>
        </div>
    );
};

export default Header;
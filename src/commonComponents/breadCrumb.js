import React, { Component } from "react";
import { ReactComponent as BackArrow } from "../assets/rightArrow.svg";
import styles from "../styles/HeaderBar.module.scss";
import { Link } from "react-router-dom";
class HeaderBar extends Component {
    render() {
        let { breadCrumb } = this.props;
        return (
            <div className={styles.container}>
                <div className={styles.HeaderBarText}>
                    {breadCrumb && breadCrumb.map((index, key) => {
                        return (
                            <span className={styles.breadCrumbSpan} key={key}>
                                <Link className={styles.breadCrumbText} to={index.to} onClick={() => this.props.onClick()}>
                                    {index.name}
                                </Link>
                                {key + 1 < breadCrumb.length && (
                                    <BackArrow className={styles.breadCrumbIcon} />
                                )}
                            </span>
                        );
                    })}
                </div>
            </div>
        );
    }
}
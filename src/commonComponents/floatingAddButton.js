import React from 'react';
import '../styles/styles.css';
import { ReactComponent as AddLogo } from '../assets/add.svg';

const FloatingAddButton = (props) => {
    return (
        <div className='floatingAddButoon' onClick={props.onClick}>
            <AddLogo />
        </div>
    );
};

export default FloatingAddButton;
import React from 'react';
import { ReactComponent as DeleteLogo } from '../assets/delete.svg';

const RoundDeleteButton = () => {
    return (
        <div className='roundButton'>
            <DeleteLogo />
        </div>
    );
};

export default RoundDeleteButton;